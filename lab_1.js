class Animal {
    create(type) {
        let curent_animal
    
        type == "cat" ? curent_animal = new cat() : curent_animal = new dog()
        
        curent_animal.say = function () {
            console.log(`${this.voise}`)
        }

        return curent_animal
    }
}

class cat {
    constructor() {
        this.voise = 'MEOW'
        this.name = 'Oleg'
    }
}

class dog {
    constructor() {
        this.voise = 'GAV GAV'
        this.name = 'Gosha'
    }
}

const factory = new Animal()
let one = factory.create("cat")

one.say()
console.log(one.name)