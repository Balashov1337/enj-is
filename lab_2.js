class PC {
    constructor(name, motherboard, cpu, graphics_card,lang) {
        this.name = name
        this.motherboard = motherboard 
        this.cpu = cpu 
        this.graphics_card = graphics_card
        this.adapter = CompilationAdapter
        this.lang = lang
    }

    compilep_programm(code) {
        let res
        if (this.lang == 'py')
             res = this.adapter.compilePY(code)
        if (this.lang == 'c')
             res = this.adapter.compileC(code)
        if (this.lang != 'c' && this.lang != 'py')
            res = "no compiler"
        return res
    }

}

class Compilation {
    static compile(code, compiler) {
        console.log("our code => ", code, "\n our compiler ==> ",compiler)
        return  this.string2Bin(code+compiler)
    }
    
    static string2Bin(str) {
        var result = [];
        for (var i = 0; i < str.length; i++) {
          result.push(str.charCodeAt(i).toString(2));
        }
        return result;
      }
}

class CompilationAdapter {
    
    static compilePY(code) {
        let res = Compilation.compile(code, 'py')
        return res
    }

    static compileC(code) {
        let res = Compilation.compile(code, 'c')
        return res
    }
}


class PCBuilder {
    constructor(name) {
        this.name = name
    }

    set_motherboard(motherboard) {
        this.motherboard = motherboard
        return this
    }

    set_cpu(cpu) {
        this.cpu = cpu
        return this
    }

    set_graphics_card(graphics_card) {
        this.graphics_card = graphics_card
        return this
    }

    setlang(lang) {
        this.lang = lang
        return this
    }

    build() {
        return new PC(this.name, this.motherboard, this.cpu, this.graphics_card, this.lang)
    }
}

const my_pc = new PCBuilder('MOnSterr!&#')
    .set_motherboard(5)
    .set_cpu(7.8)
    .set_graphics_card("23123123")
    .setlang("c")
    .build()

console.log(my_pc)
console.log(my_pc.compilep_programm("1asydgashjdkahjksd"))