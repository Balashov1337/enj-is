class ScreenFactory {
    create(type) {
        let curent_product
    
        type == "PC" ? curent_product = new PCScreen() : curent_product = new PhoneScreen()
        
        curent_product.info = function () {
            console.log(` type : ${this.type} , name : ${this.name}`)
        }

        return curent_product
    }
}

class PCScreen {
    constructor() {
        this.type = 'PC'
        this.name = 'Retina 13" '
    }
}

class PhoneScreen {
    constructor() {
        this.type = 'Phone'
        this.name = 'Retina 5.5" '
    }
}

class CPUFactory {
    create(type) {
        let curent_product
    
        type == "PC" ? curent_product = new PCCPU() : curent_product = new PhoneCPU()
        
        curent_product.info = function () {
            console.log(` type : ${this.type} , name : ${this.name}`)
        }

        return curent_product
    }
}

class PCCPU {
    constructor() {
        this.type = 'PC'
        this.name = ' Intel core i666'
    }
}

class PhoneCPU {
    constructor() {
        this.type = 'Phone'
        this.name = 'SnapDragon 888'
    }
}


class MotherboardFactory {
    create(type) {
        let curent_product
    
        type == "PC" ? curent_product = new PCMotherboard() : curent_product = new PhoneMotherboard()
        
        curent_product.info = function () {
            console.log(` type : ${this.type} , name : ${this.name}`)
        }

        return curent_product
    }
}

class PCMotherboard {
    constructor() {
        this.type = 'PC'
        this.name = 'Asus X779'
    }
}

class PhoneMotherboard {
    constructor() {
        this.type = 'Phone'
        this.name = 'Cool Phone Motherboard'
    }
}



class EventObserver {
    constructor () {
      this.observers = []
      this.checkResult = []
    }
  
    subscribe (fn) {
      this.observers.push(fn)
    }
  
    unsubscribe (fn) {
      this.observers = this.observers.filter(subscriber => subscriber !== fn)
    }
  
    broadcast (data) {
        this.checkResult = []
        for (let sub of this.observers){
            this.checkResult.push(sub.create(data))
        }
    }
  }

  const observer = new EventObserver()

  const factory1 = new ScreenFactory()
  const factory2 = new CPUFactory()
  const factory3 = new MotherboardFactory()

  observer.subscribe(factory1)
  observer.subscribe(factory2)
  observer.subscribe(factory3)
  observer.broadcast("Phone")
  observer.checkResult.forEach(i => i.info())
  observer.unsubscribe(factory3)
  observer.broadcast("PC")
  observer.checkResult.forEach(i => i.info())