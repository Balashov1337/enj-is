class HashOutput {
    alert(message) {
        console.log("#####", message)
    }
}

class StartOutput {
    alert(message) {
        console.log("****", message)
    }
}

class ShowMessage {
    constructor(output) {
        this.output = output
    }

    check (){
        if (!(this.output instanceof HashOutput || this.output instanceof StartOutput))
            this.output = new HashOutput
    }

    alert (message) {
        this.output.alert(message)
    }
}

const showMess = new ShowMessage()
showMess.check()


const showWelcome = (user) => {
    showMess.alert(`Welcome user: ${user} `)
}

const showRes = (user) => {
    showMess.alert(`RES : ${user} `)
}

showWelcome("Oleg")

